############################################
# Code to get OpenTargets data
#	using an API
#	in json format
#	and parse it
#
# Usage:
#	python3 assignment_open_targets.py
#
############################################

################################
# import libraries
################################
import json
from pprint import pprint
import pdb
import os

#################
# get the data
#################
os.system(" curl -X POST -d '{\"disease\":[\"EFO_0000253\"]}' --header 'Content-Type: application/json' https://platform-api.opentargets.io/v3/platform/public/evidence/filter\?target\=ENSG00000157764  -k > data.json  ")

# curl -X POST -d '{"disease":["EFO_0000253"]}' --header 'Content-Type: application/json' https://platform-api.opentargets.io/v3/platform/public/evidence/filter\?target\=ENSG00000157764  -k > data.json  

#################
# Open json file
#################
with open('data.json') as data_file:   
    data = json.load(data_file)

pprint(data)

# Accessing fields and elements

#data["maps"][0]["id"]
#data["masks"]["id"]
#data["om_points"]

#pdb.set_trace()
